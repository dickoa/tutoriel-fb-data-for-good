---
title: "Identifier les populations à risque à l’aide des ensembles de données publics Facebook Data For Good"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE,
                      warning = FALSE,
                      message = FALSE)
```

## Charger les packages pour l'analyse

Les packages suivant seront utiles pour cette analyse.

- le package `raster` permet de manipuler les données spatiale matricielle en grille (raster) et permet aussi de télécharger les données `GADM` en utilisant la fonction `getData`.

- le package `exactextractr` permet de faire les calculs de statistiques zonale entre une grille et une couche de données vectorielles.

- le package `rhdx` est utilisé pour obtenir directement les données de la plateforme `HDX`

- le package `cowplot` est utilisé la function `theme_map`

- le package `gganimate` est utilisé pour animer la carte

```{r}
library(raster)
library(tidyverse)
library(sf)
library(exactextractr)
library(rhdx)
library(gganimate)
library(cowplot)
```

## Choisir un pays pour l'analyse

La fonction `getData` du package `raster` peut être utilisé pour obtenir la couche de données vectorielles qu'on utilisera pour ce tutoriel.

```{r load_mli_adm3}
mli_adm3 <- getData("GADM", country = "MLI", level = 3)
mli_adm3 <- st_as_sf(mli_adm3)
glimpse(mli_adm3)
```

On peut inspecter visuellement

```{r map}
ggplot(mli_adm3) +
  geom_sf() +
  theme_map()
```


### Charger les données de densité de population

Les données de population sont disponibles sur `HDX` et peuvent être obtenu en utilisant `rhdx`.

```{r load_pop}
mli_pop <- pull_dataset("highresolutionpopulationdensitymaps-mli") |>
  get_resource(1) |>
  read_resource() |>
  as("Raster")
mli_pop
```

```{r load_pop60}
mli_pop_60 <- pull_dataset("highresolutionpopulationdensitymaps-mli") |>
  get_resource(6) |>
  read_resource() |>
  as("Raster")
mli_pop_60
```

### Charger les données d'amplitude de mouvements

```{r load_mov}
zip_file_path <- pull_dataset("movement-range-maps") |>
  get_resource(2) |>
  download_resource()

files <- unzip(zip_file_path, list = TRUE)
files <- grep("movement-range", files$Name, value = TRUE)

file_path <- unzip(zip_file_path, files = files)

mli_mov <- read_delim(file_path, delim = "\t") |>
  filter(ds >= as.Date("2021-07-25"),
         ds <= as.Date("2021-07-31"))
glimpse(mli_mov)
```

### Joindre les données de zones déplacement à la couche de polygones admin 3

```{r join}
mli_joined <- left_join(mli_adm3, mli_mov,
                        by = c("GID_2" = "polygon_id"))
```

### Calculer des statistiques de zone

```{r zonal_stat, cache = TRUE}
mli_joined$total_pop <- exact_extract(mli_pop, mli_joined,
                                      fun = 'sum',
                                      progress = FALSE)
mli_joined$elderly_pop <- exact_extract(mli_pop_60, mli_joined,
                                        fun = 'sum',
                                        progress = FALSE)
```

### Calculer le pourcentage de personnes âgées

```{r percent_eld}
mli_joined <- mli_joined |>
  mutate(percent_elderly = elderly_pop / total_pop)
```

### Calculer un score de risque pondéré

```{r risk_score}
mli_joined <- mli_joined |>
  mutate(risk_score = (percent_elderly * 100) * (100 - (all_day_ratio_single_tile_users * 100)) * elderly_pop)
```

### Calculer un score de risque gradué

```{r risk_score_scaled}
mli_joined <- mli_joined |>
  mutate(risk_score_scaled = 100 * (risk_score - min(risk_score, na.rm = TRUE)) / diff(range(risk_score, na.rm = TRUE)))
```

### Représenter la carte

```{r map_score}
ggplot(mli_joined) +
  geom_sf(aes(fill = risk_score_scaled)) +
  scale_fill_viridis_c(direction = -1) +
  theme_map()
```

### Animer la carte

```{r anim}
bbox <- st_bbox(mli_adm3)

mli_joined |>
  drop_na(ds) |>
  ggplot() +
  geom_sf(data = mli_adm3, size = 0.25) +
  geom_sf(aes(fill = risk_score_scaled), size = 0.25) +
  coord_sf(xlim = c(bbox["xmin"], bbox["xmax"]),
           ylim = c(bbox["ymin"], bbox["ymax"])) +
  scale_fill_viridis_c(direction = -1) +
  labs(title = "Date: {frame_time}") +
  theme_map() +
  transition_time(ds) +
  ease_aes("linear")
```
